// exo 1
const input = prompt("Veuillez entrer une valeur:");

const myPutStr = () => {
    if (!input || !input.trim()) {
        return alert("Erreur : valeur non valide.");
    }
    
    if (!isNaN(Number(input))) {
        return alert("et pourquoi pas 42 ?");
    }

    return alert(`Vous avez entré: ${input}`);
}

myPutStr();

// exo 2 
const longueur = parseFloat(prompt("Combien de longueur avez-vous ?"));
const largeur = parseFloat(prompt("Combien de largeur avez-vous ?"));

const computeSurfaceM2 = () => {
    if (isNaN(longueur) || isNaN(largeur)) {
        return alert("Veuillez entrer des valeurs numériques valides pour la longueur et la largeur.");
    }

    const surface = Math.round(longueur * largeur);
    alert(`Vous avez ${surface}m²`);
}

computeSurfaceM2();

//exo 3
/*
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Matrix Generator</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>

<div id="matrixContainer"></div>

<script>
    function matrixGenerator(matrix) {
        // Create the table
        const table = document.createElement('table');

        // Iterate through each row of the matrix
        for (let i = 0; i < matrix.length; i++) {
            const row = document.createElement('tr');

            // Iterate through each column of the row
            for (let j = 0; j < matrix[i].length; j++) {
                const cell = document.createElement('td');
                cell.textContent = matrix[i][j];
                row.appendChild(cell);
            }

            table.appendChild(row);
        }

        return table;
    }

    // Use the function
    const matrixData = [[1, 1, 1, 1, 1], [0, 1, 0, 1, 0], [1, 0, 0, 1, 1]];
    const matrixContainer = document.getElementById('matrixContainer');
    matrixContainer.appendChild(matrixGenerator(matrixData));

</script>

</body>
</html>

*/
 
// exo 5

/*<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Horloge Numérique</title>
</head>
<body>

<div id="horloge">00:00:00</div>
</body>
</html>

const miseAJourHorloge = () => {
        const maintenant = new Date();
        const heures = String(maintenant.getHours()).padStart(2, '0');
        const minutes = String(maintenant.getMinutes()).padStart(2, '0');
        const secondes = String(maintenant.getSeconds()).padStart(2, '0');
        document.getElementById('horloge').textContent = `${heures}:${minutes}:${secondes}`;
    }

    setInterval(miseAJourHorloge, 1000);
    */

   // exo 6 

   // exo 7
   const fibonacci = (n) => {
    if (n <= 1) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}


const FibonacciGenerator = (n)  => {
    let series = [];
    for (let i = 0; i < n; i++) {
        series.push(fibonacci(i));
    }
    return series;
}

const disorderlyOrder = (arr) => {
    return arr.sort((a, b) => b - a);
}

const addictions = (arr) => {
    return arr.reduce((acc, curr) => acc + curr, 0);
}

const n = 10;
const fibo = FibonacciGenerator(n);
console.log("c:", fibo);

const decroissant = sortDescending(fibSeries);
console.log("Série triée de l'ordre désordonne :", decroissant);

const totalSum = addictions(sortedSeries);
console.log("Somme totale :",  totalSum);

