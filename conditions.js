//Exo 1
let age = prompt("Bonjour cher(e) noctambule ! Avant de plonger dans le tourbillon de la nuit, pouvons-nous savoir si tu as assez d’années au compteur pour danser avec les étoiles ? 🌠🎉 S'il te plaît, partage ton millésime de naissance !");
if (age < 18) {
    console.log("Vous ne pouvez pas entrez vous n’êtes pas majeur vous avez" + age )
} else if  (age => 42) {
    console.log("Il est devenu le patron, ayant franchi le cap des " + age)

} else {
    console.log("Vous pouvez entrer vous êtes majeur vous avez" + age)
}

// Exo 2
const rand = Math.floor(Math.random() * 30)
console.log(rand)
if (rand <= 10) {
    console.log("cool")
} else if (rand >= 11 && rand <= 20) {
   console.log("Tepid") 
} else if (rand >= 21 && rand < 30) {
    console.log("Warm")
}

// Exo 3
const days = new Date()
const j = days.getDay();
console.log(j)

switch (j) {
  case 1:
    console.log("lundi");
    break;
  case 2:
    console.log("mardi");
    break;
  case 3:
    console.log("mercredi");
    break; 
  case 4:
    console.log("jeudi");
    break;
  case 5:
    console.log("vendredi");
    break;
  case 6:
    console.log("samedi");
    break;
  case 0:
    console.log("dimanche");
    break;
  default:
    console.log(`Il y a un problème parce qu'il y a pas plus de 7 jours dans la semaine.`);
}

// Exo 4
let porte = prompt("Devant vous, trois portes: Sagesse, Connaissance, Inconnu. Laquelle choisissez-vous?");

if (porte === "Sagesse" || porte === "S" || porte === "s") {
    let choix = prompt("Un sage apparaît. Voulez-vous écouter ses conseils (C) ou le questionner (Q)?");
    
    if (choix === "C" || choix === "c") {
        window.alert("Vous écoutez attentivement. Vous gagnez en sagesse et vivez en paix pour le reste de vos jours.");
    } else if (choix === "Q" || choix === "q") {
        window.alert("Vous posez des questions et le sage partage ses connaissances. Vous devenez plus sage et éclairé.");
    } else {
        window.alert("Vous n'avez pas fait un choix valable et le sage disparaît, vous laissant seul avec vos pensées.");
    }
    
} else if (porte === "Connaissance" || porte === "C" || porte === "c") {
    let choix = prompt("Vous découvrez une bibliothèque infinie. Voulez-vous lire un livre ancien (A) ou un livre moderne (M)?");
    
    if (choix === "A" || choix === "a") {
        window.alert("Vous apprenez des secrets anciens et devenez un érudit respecté.");
    } else if (choix === "M" || choix === "m") {
        window.alert("Vous apprenez des théories nouvelles et révolutionnaires et devenez un innovateur renommé.");
    } else {
        window.alert("Vous êtes dépassé par le choix et quittez la bibliothèque sans avoir lu un seul livre.");
    }

} else if (porte === "Inconnu" || porte === "I" || porte === "i") {
    let choix = prompt("Vous êtes dans un espace infini. Voulez-vous aller vers la lumière (L) ou vers l'obscurité (O)?");
    
    if (choix === "L" || choix === "l") {
        window.alert("Vous trouvez la paix et la sérénité dans la lumière éternelle.");
    } else if (choix === "O" || choix === "o") {
        window.alert("La grande réponse sur la vie, l’univers et le reste !");
    } else {
        window.alert("Perdu entre la lumière et l'obscurité, vous errevez dans l'inconnu pour l'éternité.");
    }

} else {
    window.alert("Choix non reconnu. Veuillez choisir entre Sagesse, Connaissance et Inconnu.");
}


// Exo 5 

//Early return" en programmation signifie terminer une fonction et retourner une valeur dès que possible, plutôt que de garder plusieurs niveaux d'imbrication ou de multiples conditions if-else. Cela peut rendre le code plus lisible et efficace.

// Exo 6 

const exo6 = true 
console.log(typeof exo6 === 'undefined' ? "Cette variable n'existe pas" : '42');

// Exo 7
let famille = Math.floor(Math.random() * 40)
console.log(famille)
let dizaine = Math.floor(famille / 10) *10; 
console.log(dizaine)
switch (dizaine) {
    case 0:
        console.log("Ce chiffre fait partie de la famille des 0");
        break;
    case 10:
        console.log("Ce chiffre fait partie de la famille des 10");
        break;
    case 20:
        console.log("Ce chiffre fait partie de la famille des 20");
        break;
    case 30:
        console.log("Ce chiffre fait partie de la famille des 30");
        break;
    default:
        console.log("Ce chiffre fait partie de la famille des 40");
        break;
}
