// Exo 1
for (let a = 1; a < 11; a++) {
    console.log(`La table de ${a}`)
    for (let b = 0; b < 11; b++) {
        let result = a*b
        console.log(`${a} * ${b} = ${result}`)
        
    }
}

/* Exo 2
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Table de Multiplication</title>
</head>
<body>
    <ul id="multiplicationTable"></ul>

  <script>
  
        let tableNumber = 5;
        let listItemsString = '';

        for (let i = 1; i <= 10; i++) {
            let result = tableNumber * i;
            listItemsString += `<li>${tableNumber} x ${i} = ${result}</li>`;
        }

   document.getElementById('multiplicationTable').innerHTML = listItemsString;
    
  </script>
</body>
</html> */
 
 // Exo 3 

 let a = 0

 while (true) {
   a++;
    console.log(`5 * ${a} = ${5 * a}`);
 }
 
// exo 4
const tableau = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const tableauMulti = tableau.map((x) => x * 5);


console.log(tableauMulti);

// exo 5

const somme = {a: "42", b: "42"}

let resultat = parseInt(somme.a) * parseInt(somme.b);
console.log(resultat);

// exo 6

const array2 = [5, 4, 3, 2, 1]

console.log(array2.sort());

// exo 7

/*<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Produits</title>
</head>
<body>

<div id="listeProduits"></div>

</body>
</html>

const produits = [
        {
            nom: "Produit 1",
            prix: 50,
            description: "Description du produit 1",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["S", "M", "L"],
            couleurs: ["Rouge", "Bleu"]
        },
{
            nom: "Produit 2",
            prix: 500,
            description: "Description du produit 2",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["M", "L", "S"],
            couleurs: ["Rouge", "Bleu","vert"]
        },
          {
            nom: "Produit 3",
            prix: 50,
            description: "Description du produit 1",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["S", "M", "L"],
            couleurs: ["Rouge", "Bleu"]
        },
{
            nom: "Produit 4",
            prix: 500,
            description: "Description du produit 2",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["M", "L", "S"],
            couleurs: ["Rouge", "Bleu","vert"]
        },
          {
            nom: "Produit 5",
            prix: 50,
            description: "Description du produit 1",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["S", "M", "L"],
            couleurs: ["Rouge", "Bleu"]
        },
{
            nom: "Produit 6",
            prix: 500,
            description: "Description du produit 2",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["M", "L", "S"],
            couleurs: ["Rouge", "Bleu","vert"]
        },
          {
            nom: "Produit 7",
            prix: 50,
            description: "Description du produit 1",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["S", "M", "L"],
            couleurs: ["Rouge", "Bleu"]
        },
{
            nom: "Produit 8",
            prix: 500,
            description: "Description du produit 2",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["M", "L", "S"],
            couleurs: ["Rouge", "Bleu","vert"]
        },
  {
            nom: "Produit 8",
            prix: 500,
            description: "Description du produit 2",
            image: "lien_image1.jpg",
            lien: "lien_produit1.html",
            tailles: ["M", "L", "S"],
            couleurs: ["Rouge", "Bleu","vert"]
        },
    ];

    const divProduits = document.getElementById('listeProduits');

    produits.forEach(produit => {
        const produitDiv = `
            <div class="produit">
                <h2>${produit.nom}</h2>
                <img src="${produit.image}" alt="${produit.nom}">
                <p>${produit.description}</p>
                <p>Prix: ${produit.prix}€</p>
                <p>Tailles: ${produit.tailles.join(", ")}</p>
                <p>Couleurs: ${produit.couleurs.join(", ")}</p>
                <a href="${produit.lien}">Voir le produit</a>
            </div>
        `;
        divProduits.innerHTML += produitDiv;
    });
*/

// exo 8

/*
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Années</title>
</head>
<body>

<select id="annees"></select>

<script>
    const anneeActuelle = new Date().getFullYear();
    let options = "";

    for (let i = 1980; i <= anneeActuelle; i++) {
        options += `<option value="${i}">${i}</option>`;
    }

    document.getElementById('annees').innerHTML = options;
</script>

</body>
</html>
*/

// exo 9

/*
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Produits</title>
</head>
<body>

<input type="text" id="recherche" placeholder="Rechercher un produit...">

<div id="listeProduits"></div>

</body>
</html>

const produits = [
    {
        nom: "Coca-Cola",
        prix: 1.50,
        description: "Soda classique au cola.",
        image: "lien_image_coca.jpg",
        lien: "lien_coca.html",
        tailles: ["33cl", "50cl", "1.5L"],
        couleurs: ["Rouge", "Blanc"]
    },
    {
        nom: "Pepsi",
        prix: 1.40,
        description: "Une autre boisson au cola.",
        image: "lien_image_pepsi.jpg",
        lien: "lien_pepsi.html",
        tailles: ["33cl", "50cl", "1.5L"],
        couleurs: ["Bleu", "Blanc", "Rouge"]
    },
    {
        nom: "Sprite",
        prix: 1.30,
        description: "Soda rafraîchissant au citron et à la lime.",
        image: "lien_image_sprite.jpg",
        lien: "lien_sprite.html",
        tailles: ["33cl", "50cl", "1.5L"],
        couleurs: ["Vert", "Blanc"]
    },
    {
        nom: "Dr. Pepper",
        prix: 1.60,
        description: "Soda original avec 23 saveurs différentes.",
        image: "lien_image_drpepper.jpg",
        lien: "lien_drpepper.html",
        tailles: ["33cl", "50cl"],
        couleurs: ["Marron", "Blanc"]
    },
    {
        nom: "Fanta",
        prix: 1.35,
        description: "Boisson gazeuse fruitée.",
        image: "lien_image_fanta.jpg",
        lien: "lien_fanta.html",
        tailles: ["33cl", "50cl", "1.5L"],
        couleurs: ["Orange", "Bleu", "Vert"]
    },
    {
        nom: "7UP",
        prix: 1.20,
        description: "Boisson gazeuse au citron vert.",
        image: "lien_image_7up.jpg",
        lien: "lien_7up.html",
        tailles: ["33cl", "50cl"],
        couleurs: ["Vert", "Blanc"]
    },
    {
        nom: "Mountain Dew",
        prix: 1.25,
        description: "Soda pétillant à la saveur d'agrumes.",
        image: "lien_image_mountaindew.jpg",
        lien: "lien_mountaindew.html",
        tailles: ["33cl", "50cl"],
        couleurs: ["Vert", "Jaune"]
    },
    {
        nom: "Root Beer",
        prix: 1.55,
        description: "Soda traditionnel américain à la salsepareille.",
        image: "lien_image_rootbeer.jpg",
        lien: "lien_rootbeer.html",
        tailles: ["33cl", "50cl"],
        couleurs: ["Marron"]
    },
    {
        nom: "Ginger Ale",
        prix: 1.40,
        description: "Soda au gingembre, parfait pour les cocktails.",
        image: "lien_image_gingerale.jpg",
        lien: "lien_gingerale.html",
        tailles: ["33cl", "50cl"],
        couleurs: ["Or", "Blanc"]
    }
];

document.getElementById('recherche').addEventListener('input', function(e) {
    const recherche = e.target.value.toLowerCase();
    const divProduits = document.getElementById('listeProduits');

    divProduits.innerHTML = '';

    if (recherche.length < 3) {
        produits.forEach(ajouterProduit);
        return;
    }

    produits.filter(produit => produit.nom.toLowerCase().startsWith(recherche)).forEach(ajouterProduit);
});

function ajouterProduit(produit) {
    const divProduits = document.getElementById('listeProduits');
    const produitDiv = `
        <div class="produit">
            <h2>${produit.nom}</h2>
            <img src="${produit.image}" alt="${produit.nom}">
            <p>${produit.description}</p>
            <p>Prix: ${produit.prix}€</p>
            <p>Tailles: ${produit.tailles.join(", ")}</p>
            <p>Couleurs: ${produit.couleurs.join(", ")}</p>
            <a href="${produit.lien}">Voir le produit</a>
        </div>
    `;
    divProduits.innerHTML += produitDiv;
}

produits.forEach(ajouterProduit);
*/

let n = 5;
for (let i = 1; i <= 1 * n; i += 2) {
    console.log('1'.repeat(i));
}

console.log("-------------------------------");

let n1 = 4;
for (let i = 2; i <= 2 * n1; i += 2) {
    console.log('1'.repeat(i));
}
