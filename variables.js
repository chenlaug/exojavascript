// Exo 1
// My school Forever

// Ex 2
/* My school forever 
 every day */

 // Exo 3
const nom = "Émile Zola"
const nbr = 42
const nbVirgule = 1.2
const vivant = false
const tableau = ["Germinal","La Bête humaine","Au Bonheur des Dames"]
const heure = Date()
const livre = {
    Titre:"la bete humaine",
    auteur: "emie zola",
    DateDePublication: 1890
}
const udn = undefined
const nul = null

//Ex4
const my42count = "quarante-deux"

//Ex5
const lolo = 42
console.log(lolo)
let variable = window['maVariable'] || 42;
console.log(variable)

//Ex6
let myArray42 = ['q',"u","a","r","a","n","t","e","-","d","e","u","x"]


//Ex7
const myArray42Len = my42count.length;

//Ex8
myArray42 = myArray42.join('') + " La grande réponse sur la vie, l'univers et le reste !"
console.log(myArray42);

//Ex9
let rand = Math.floor( Math.random() * 43)
if (rand === 42) {
    rand = true
}

//Ex10
const my42String = "42"
const my42Nombre = 42
const my42Float = 42.42
const my42Array = [4,2]
const my42Date = Date()
const my42Objet = {nb:"42"}
const my42Undefined  = undefined
const my42Null = null


//Exo11
const compute42 = 2 * 21 
compute42String = compute42.toString()

//Exo12
let exo12 = "42424242"
exo12 = "quarante-deux"

//Exo13
let a = 24
let b = 42
let temp = a;
a = b;
b = temp;
