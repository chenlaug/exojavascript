# Collection d'Exercices JavaScript

Dans ce dépôt, j'ai regroupé 4 fichiers centrés autour des concepts de base de JavaScript.

## Fichiers :

1. `boucles.js` - Exercices liés aux différentes boucles en JavaScript.
2. `conditions.js` - Exercices traitant des structures conditionnelles.
3. `fonctions.js` - Exercices sur la définition et l'utilisation des fonctions.
4. `variables.js` - Exercices sur la déclaration et l'utilisation de variables.

## Comment j'utilise ce dépôt

Je clone ce dépôt et j'ouvre les fichiers dans mon éditeur de code pour étudier ou modifier le code. Ces fichiers m'aident à renforcer mes compétences dans les concepts fondamentaux de JavaScript.
